package com.main.workspace.dao;

import com.main.workspace.model.Course;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CourseDAO {

    private final Connection connection;

    public CourseDAO(Connection connection) {
        this.connection = connection;
    }

    public List<Course> getCourses() throws SQLException {
        List<Course> courses = new ArrayList<>();

        try (PreparedStatement stmt = connection.prepareStatement("SELECT CourseID, CourseName FROM course")) {
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    courses.add(new Course(rs.getString("CourseID"), rs.getString("CourseName")));
                }
            }
        } catch (SQLException e) {
            printSQLException(e);
        }

        return courses;
    }

    public Course getCourseById(String id) {
        Course course = null;
        String sql = "SELECT * FROM course WHERE CourseID = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                String courseName = resultSet.getString("CourseName");
                course = new Course(id, courseName);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }

        return course;
    }

    public List<Course> getAllCourses() throws SQLException {
        List<Course> courses = new ArrayList<>();
        String sql = "SELECT CourseID, CourseName FROM course";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String courseId = resultSet.getString("CourseID");
                String courseName = resultSet.getString("CourseName");
                courses.add(new Course(courseId, courseName));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }

        return courses;
    }

    public List<Course> getCourseByStudentId(String studentId) throws SQLException {
        List<Course> courses = new ArrayList<>();
        String sql = "SELECT c.CourseID, c.CourseName FROM course c JOIN studentcourse sc ON c.CourseID = sc.CourseID WHERE sc.StudentID = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, studentId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String courseId = resultSet.getString("CourseID");
                String courseName = resultSet.getString("CourseName");
                courses.add(new Course(courseId, courseName));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }

        return courses;
    }

    public void updateCourse(Course course) throws SQLException {
        String sql = "UPDATE course SET CourseName = ? WHERE CourseID = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, course.getCourseName());
            preparedStatement.setString(2, course.getCourseId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    public void deleteCourse(String id) throws SQLException {
        String sql = "DELETE FROM course WHERE CourseID = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    public void createCourse(Course course) throws SQLException {
        String sql = "INSERT INTO course (CourseID, CourseName) VALUES (?, ?)";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, course.getCourseId());
            preparedStatement.setString(2, course.getCourseName());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    public void removeStudent(String studentID, String courseID) throws SQLException {
        String sql = "DELETE FROM studentcourse WHERE StudentID = ? AND CourseID = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, studentID);
            preparedStatement.setString(2, courseID);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
