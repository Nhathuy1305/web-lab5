package com.main.workspace.dao;

import com.main.workspace.model.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class StudentDAO {

    private final Connection connection;

    public StudentDAO(Connection connection) {
        this.connection = connection;
    }

    public List<Student> getStudents() throws SQLException {
        List<Student> students = new ArrayList<>();

        try (PreparedStatement stmt = connection.prepareStatement("SELECT StudentID, StudentName FROM student")) {
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    students.add(new Student(rs.getString("StudentID"), rs.getString("StudentName")));
                }
            }
        } catch (SQLException e) {
            printSQLException(e);
        }

        return students;
    }

    public Student getStudentById(String id) {
        Student student = null;
        String sql = "SELECT * FROM student WHERE studentID = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                String studentName = resultSet.getString("studentName");
                student = new Student(id, studentName);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }

        return student;
    }

    public List<Student> getStudentsByCourseId(String courseID) {
        List<Student> students = new ArrayList<>();
        String sql = "SELECT student.studentID, student.studentName FROM student INNER JOIN studentcourse ON student.studentID = studentcourse.studentID WHERE studentcourse.courseID = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, courseID);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                students.add(new Student(resultSet.getString("studentID"), resultSet.getString("studentName")));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }

        return students;
    }

    public void updateStudent(Student student) throws SQLException {
        String sql = "UPDATE student SET studentName = ? WHERE studentID = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, student.getName());
            preparedStatement.setString(2, student.getId());

            preparedStatement.executeUpdate();
        }
    }

    public void deleteStudent(String id) throws SQLException {
        String sql = "DELETE FROM student WHERE studentID = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, id);

            preparedStatement.executeUpdate();
        }
    }

    public void createStudent(Student student) throws SQLException {
        String sql = "INSERT INTO student (studentID, studentName) VALUES (?, ?)";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, student.getId());
            preparedStatement.setString(2, student.getName());

            preparedStatement.executeUpdate();
        }
    }

    public void addCourse(String studentID, String courseID) throws SQLException {
        String sql = "INSERT INTO studentcourse (studentID, courseID) VALUES (?, ?)";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, studentID);
            preparedStatement.setString(2, courseID);

            preparedStatement.executeUpdate();
        }
    }

    public void removeCourse(String studentID, String courseID) throws SQLException {
        String sql = "DELETE FROM studentcourse WHERE studentID = ? AND courseID = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, studentID);
            preparedStatement.setString(2, courseID);

            preparedStatement.executeUpdate();
        }
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
