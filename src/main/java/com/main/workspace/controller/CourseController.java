package com.main.workspace.controller;

import com.main.workspace.model.Course;
import com.main.workspace.dao.CourseDAO;
import com.main.workspace.util.ConnectionPool;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "CourseController", urlPatterns = {"/Course"})
public class CourseController extends HttpServlet {

    private CourseDAO courseDAO;

    @Override
    public void init() throws ServletException {
        try {
            courseDAO = new CourseDAO(ConnectionPool.getConnection());
        } catch (SQLException e) {
            System.err.println("SQLException in init(): " + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Course> courses;
        try {
            CourseDAO courseDAO = new CourseDAO(ConnectionPool.getConnection());
            courses = courseDAO.getCourses();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        request.setAttribute("courses", courses);
        RequestDispatcher dispatcher = request.getRequestDispatcher("Course/index.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if ("update".equals(action)) {
            handleUpdate(request, response);
        } else if ("delete".equals(action)) {
            handleDelete(request, response);
        } else if ("create".equals(action)) {
            handleCreate(request, response);
        } else if ("removeStudent".equals(action)) {
            handleRemoveStudent(request, response);
        } else {
            throw new IllegalArgumentException("Invalid action: " + action);
        }
    }

    private void handleUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String originalCourseId = request.getParameter("id");
        String courseName = request.getParameter("courseName");

        Course course = new Course(originalCourseId, courseName);

        try {
            courseDAO.updateCourse(course);
            request.setAttribute("message", "Course updated successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
            request.setAttribute("message", "Error updating course: " + e.getMessage());
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/Course/success.jsp");
        dispatcher.forward(request, response);
    }

    private void handleDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String courseId = request.getParameter("id");

        try {
            courseDAO.deleteCourse(courseId);
            request.setAttribute("message", "Course deleted successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
            request.setAttribute("message", "Error deleting course: " + e.getMessage());
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/Course/success.jsp");
        dispatcher.forward(request, response);
    }

    private void handleCreate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String courseId = request.getParameter("id");
        String courseName = request.getParameter("courseName");

        Course course = new Course(courseId, courseName);

        try {
            courseDAO.createCourse(course);
            request.setAttribute("message", "Course created successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
            request.setAttribute("message", "Error creating course: " + e.getMessage());
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/Course/success.jsp");
        dispatcher.forward(request, response);
    }

    private void handleRemoveStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String studentID = request.getParameter("studentID");
        String courseID = request.getParameter("courseID");

        try {
            courseDAO.removeStudent(studentID, courseID);
            request.setAttribute("message", "Student removed from course successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
            request.setAttribute("message", "Error removing student from course: " + e.getMessage());
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/Course/course-detail.jsp");
        dispatcher.forward(request, response);
    }
}