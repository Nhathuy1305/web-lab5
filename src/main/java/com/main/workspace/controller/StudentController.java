package com.main.workspace.controller;

import com.main.workspace.dao.StudentDAO;
import com.main.workspace.model.Student;
import com.main.workspace.util.ConnectionPool;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.List;

@WebServlet(name = "StudentController", urlPatterns = {"/Student"})
public class StudentController extends HttpServlet {

    private StudentDAO studentDAO;

    @Override
    public void init() throws ServletException {
        try {
            studentDAO = new StudentDAO(ConnectionPool.getConnection());
        } catch (SQLException e) {
            System.err.println("SQLException in init(): " + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Student> students;
        try {
            students = studentDAO.getStudents();
            System.out.println("Retrieved students: " + students);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        request.setAttribute("students", students);
        RequestDispatcher dispatcher = request.getRequestDispatcher("Student/index.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if ("update".equals(action)) {
            handleUpdate(request, response);
        } else if ("delete".equals(action)) {
            handleDelete(request, response);
        } else if ("create".equals(action)) {
            handleCreate(request, response);
        } else if ("view".equals(action)) {
            handleView(request, response);
        } else if ("addCourse".equals(action)) {
            handleAddCourse(request, response);
        } else if ("removeCourse".equals(action)) {
            handleRemoveCourse(request, response);
        } else {
            throw new IllegalArgumentException("Invalid action: " + action);
        }
    }

    private void handleUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String originalStudentID = request.getParameter("id"); // For filtering
        String studentName = request.getParameter("studentName");

        Student student = new Student(originalStudentID, studentName);

        try {
            studentDAO.updateStudent(student);
            request.setAttribute("message", "Student updated successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
            request.setAttribute("message", "Error updating student: " + e.getMessage());
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/Student/success.jsp");
        dispatcher.forward(request, response);
    }

    private void handleDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String studentID = request.getParameter("id");

        try {
            studentDAO.deleteStudent(studentID);
            request.setAttribute("message", "Student deleted successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
            request.setAttribute("message", "Error deleting student: " + e.getMessage());
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/Student/success.jsp");
        dispatcher.forward(request, response);
    }

    private void handleCreate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String studentID = request.getParameter("id");
        String studentName = request.getParameter("studentName");

        Student student = new Student(studentID, studentName);

        try {
            studentDAO.createStudent(student);
            request.setAttribute("message", "Student created successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
            request.setAttribute("message", "Error creating student: " + e.getMessage());
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/Student/success.jsp");
        dispatcher.forward(request, response);
    }

    private void handleView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String studentID = request.getParameter("id");

        Student student = studentDAO.getStudentById(studentID);

        if (student != null) {
            request.setAttribute("student", student);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Student/view.jsp");
            dispatcher.forward(request, response);
        } else {
            request.setAttribute("message", "No student found with ID: " + studentID);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Student/error.jsp");
            dispatcher.forward(request, response);
        }
    }

    private void handleAddCourse(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String studentID = request.getParameter("id");
        String courseID = request.getParameter("courseID");

        try {
            studentDAO.addCourse(studentID, courseID);
            request.setAttribute("message", "Course added successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
            request.setAttribute("message", "Error adding course: " + e.getMessage());
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/Student/view.jsp");
        dispatcher.forward(request, response);
    }

    private void handleRemoveCourse(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String studentID = request.getParameter("id");
        String courseID = request.getParameter("courseID");

        try {
            studentDAO.removeCourse(studentID, courseID);
            request.setAttribute("message", "Course removed successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
            request.setAttribute("message", "Error removing course: " + e.getMessage());
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/Student/view.jsp");
        dispatcher.forward(request, response);
    }
}