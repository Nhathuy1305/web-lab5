<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Error</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<h2>Error</h2>
<p style="text-align: center; color: red;"><%= request.getAttribute("errorMessage") %></p>
<div style="text-align: center">
    <a href="${pageContext.request.contextPath}/">Return to Homepage</a>
</div>
</body>
</html>