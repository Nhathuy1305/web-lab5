<%@ page import="com.main.workspace.model.Course" %>
<%@ page import="com.main.workspace.model.Student" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.main.workspace.dao.StudentDAO" %>
<%@ page import="com.main.workspace.util.ConnectionPool" %>
<%@ page import="java.util.List" %>
<%@ page import="com.main.workspace.dao.CourseDAO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String studentId = request.getParameter("studentID");
    StudentDAO studentDAO;

    try {
        studentDAO = new StudentDAO(ConnectionPool.getConnection());
    } catch (SQLException e) {
        throw new RuntimeException(e);
    }

    Student student;
    student = studentDAO.getStudentById(studentId);

    CourseDAO courseDAO;
    try {
        courseDAO = new CourseDAO(ConnectionPool.getConnection());
    } catch (SQLException e) {
        throw new RuntimeException(e);
    }

    List<Course> courses;
    try {
        courses = courseDAO.getAllCourses();
    } catch (SQLException e) {
        throw new RuntimeException(e);
    }

    List<Course> registeredCourses;
    try {
        registeredCourses = courseDAO.getCourseByStudentId(studentId);
    } catch (SQLException e) {
        throw new RuntimeException(e);
    }
%>
<!DOCTYPE html>
<html>
<head>
    <title>Student Details</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<h2>Student Details</h2>
<p style="text-align: center">Student ID: <%= student.getId() %></p>
<p style="text-align: center">Student Name: <%= student.getName() %></p>

<h2>Select Course</h2>
<form style="text-align: center" action="${pageContext.request.contextPath}/Student?action=addCourse&studentID=<%= student.getId() %>" method="post" onsubmit="return confirmAddCourse()">
    <input type="hidden" name="id" value="<%= student.getId() %>">
    Course:
    <label>
        <select name="courseID">
            <option>Select Course</option>
            <% for (Course course : courses) { %>
            <option value="<%=course.getCourseId()%>"><%=course.getCourseName()%></option>
            <% } %>
        </select>
    </label>
    <button type="submit">Add</button>
</form>

<h2>Registered Courses:</h2>
<table>
    <thead>
    <tr>
        <th>Course ID</th>
        <th>Course Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <% for (Course registeredCourse : registeredCourses) { %>
    <tr>
        <td><%= registeredCourse.getCourseId() %></td>
        <td><%= registeredCourse.getCourseName() %></td>
        <td>
            <form action="${pageContext.request.contextPath}/Student?action=removeCourse&studentID=<%= student.getId() %>" method="post" onsubmit="return confirmRemoveCourse()">
                <input type="hidden" name="id" value="<%= student.getId() %>">
                <input type="hidden" name="courseID" value="<%= registeredCourse.getCourseId() %>">
                <button type="submit">Remove</button>
            </form>
        </td>
    </tr>
    <% } %>
    </tbody>
</table>
<hr>
<div style="text-align: center">
    <a href="${pageContext.request.contextPath}/">Homepage</a>
</div></body>

<script>
    function confirmAddCourse() {
        return confirm('Are you sure you want to add this course?');
    }

    function confirmRemoveCourse() {
        return confirm('Are you sure you want to remove this course?');
    }
</script>

</html>
