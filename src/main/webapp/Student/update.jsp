<%@ page import="com.main.workspace.model.Student" %>
<%@ page import="com.main.workspace.dao.StudentDAO" %>
<%@ page import="com.main.workspace.util.ConnectionPool" %>
<%@ page import="java.sql.SQLException" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
  String studentId = request.getParameter("id");
    StudentDAO studentDAO;
    try {
        studentDAO = new StudentDAO(ConnectionPool.getConnection());
    } catch (SQLException e) {
        throw new RuntimeException(e);
    }
    Student student = studentDAO.getStudentById(studentId);
%>
<!DOCTYPE html>
<html>
<head>
  <title>Update Student</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<h2>Update Student</h2>
<%
  if (student != null) {
%>
<form class="update-form" action="${pageContext.request.contextPath}/Student?action=update" method="post">
  <input type="hidden" name="id" value="<%= student.getId() %>">
  <label style="text-align: center" for="studentName">Student Name</label><br>
  <input type="text" id="studentName" name="studentName" value="<%= student.getName() %>"><br>
  <input class="submit" type="submit" value="Update">
</form>
<%
} else {
%>
<p>No student found with the given ID.</p>
<%
  }
%>
</body>
</html>