<%@ page import="com.main.workspace.model.Student" %>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Student List</title>
    <link rel="stylesheet" type="text/css" href="Student/style.css">
</head>
<body>
<h2>Student List</h2>
<%
    List<com.main.workspace.model.Student> students = (List<Student>) request.getAttribute("students");
    if (students != null && !students.isEmpty()) {
%>
<table>
  <thead>
  <tr>
    <th>Student ID</th>
    <th>Student Name</th>
    <th>Action</th>
  </tr>
  </thead>
  <tbody>
  <% for (Student student : students) { %>
<tr>
  <td><%=student.getId()%></td>
  <td><%=student.getName()%></td>
  <td>
      <a href="Student/update.jsp?id=<%=student.getId()%>">Edit</a> |
      <a href="Student/delete.jsp?id=<%=student.getId()%>">Delete</a>
  </td>
</tr>
<% }
  %>
  </tbody>
</table>
<% } else { %>
<p>No students found.</p>
<% } %>

<a class="center-button" href="Student/create.jsp">New student</a><br>
<hr>
<a class="center-button" href="${pageContext.request.contextPath}/">Homepage</a>

</body>
</html>