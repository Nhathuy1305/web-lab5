<%@ page import="com.main.workspace.model.Student" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.main.workspace.dao.StudentDAO" %>
<%@ page import="com.main.workspace.util.ConnectionPool" %><%
    String studentId = request.getParameter("id");
    StudentDAO studentDAO;
    try {
        studentDAO = new StudentDAO(ConnectionPool.getConnection());
    } catch (SQLException e) {
        throw new RuntimeException(e);
    }
    Student student = studentDAO.getStudentById(studentId);
%>
<!DOCTYPE html>
<html>
<head>
    <title>Delete Student</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript">
        function confirmDelete() {
            return confirm('Are you sure you want to delete this student?');
        }
    </script>
</head>
<body>
<h2 class="delete-message">Delete student</h2>
<h3 class="delete-message"><%= student.getName()%> (<%= student.getId() %>)</h3>
<form action="${pageContext.request.contextPath}/Student?action=delete" method="post" onsubmit="return confirmDelete()">
    <input type="hidden" name="id" value="<%= studentId %>">
    <input class="delete-button" type="submit" value="Delete">
</form>
</body>
</html>