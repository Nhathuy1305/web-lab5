<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create Student</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<h2>Create Student</h2>
<form class="create-form" action="${pageContext.request.contextPath}/Student?action=create" method="post">
    <input type="hidden" name="action" value="create">
    <label for="id">Student ID:</label><br>
    <input type="text" id="id" name="id"><br>
    <label for="studentName">Student Name:</label><br>
    <input type="text" id="studentName" name="studentName"><br>
    <input class="create-button" type="submit" value="Create Student">
</form>
</body>
</html>