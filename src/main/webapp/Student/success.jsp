<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String message = (String) request.getAttribute("message");
%>
<html>
<head>
    <title>Operation Status</title>
    <link rel="stylesheet" type="text/css" href="Student/style.css">
</head>
<body>
<h1 class="message"><%= message %></h1>
<a class="back-link" href="${pageContext.request.contextPath}/Student">Back to Student List</a>
</body>
</html>