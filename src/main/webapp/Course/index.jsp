<%@ page import="com.main.workspace.model.Course" %>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Course List</title>
    <link rel="stylesheet" type="text/css" href="Course/style.css">
</head>
<body>
<h2>Course List</h2>
<%
    List<com.main.workspace.model.Course> courses = (List<Course>) request.getAttribute("courses");
    if (courses != null && !courses.isEmpty()) {
%>
<table>
    <thead>
    <tr>
        <th>Course ID</th>
        <th>Course Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <% for (Course course : courses) { %>
    <tr>
        <td><%=course.getCourseId()%></td>
        <td><a href="${pageContext.request.contextPath}/Course/course-detail.jsp?courseID=<%=course.getCourseId()%>"><%=course.getCourseName()%></a></td>
        <td>
            <a href="Course/update.jsp?id=<%=course.getCourseId()%>">Edit</a> |
            <a href="Course/delete.jsp?id=<%=course.getCourseId()%>">Delete</a>
        </td>
    </tr>
    <% }
    %>
    </tbody>
</table>
<% } else { %>
<p>No courses found.</p>
<% } %>

<a class="center-button" href="Course/create.jsp">New course</a><br>
<hr>
<a class="center-button" href="${pageContext.request.contextPath}/">Homepage</a>

</body>
</html>