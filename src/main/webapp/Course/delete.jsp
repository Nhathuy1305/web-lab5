<%@ page import="com.main.workspace.model.Course" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.main.workspace.dao.CourseDAO" %>
<%@ page import="com.main.workspace.util.ConnectionPool" %><%
    String courseId = request.getParameter("id");
    CourseDAO courseDAO;
    try {
        courseDAO = new CourseDAO(ConnectionPool.getConnection());
    } catch (SQLException e) {
        throw new RuntimeException(e);
    }
    Course course = courseDAO.getCourseById(courseId);
%>
<!DOCTYPE html>
<html>
<head>
    <title>Delete Course</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript">
        function confirmDelete() {
            return confirm('Are you sure you want to delete this course?');
        }
    </script>
</head>
<body>
<h2 class="delete-message">Delete course</h2>
<h3 class="delete-message"><%= course.getCourseName()%> (<%= course.getCourseId() %>)</h3>
<form action="${pageContext.request.contextPath}/Course?action=delete" method="post" onsubmit="return confirmDelete()">
    <input type="hidden" name="id" value="<%= courseId %>">
    <input class="delete-button" type="submit" value="Delete">
</form>
</body>
</html>