<%@ page import="com.main.workspace.dao.CourseDAO" %>
<%@ page import="com.main.workspace.util.ConnectionPool" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.main.workspace.model.Course" %>
<%@ page import="com.main.workspace.dao.CourseDAO" %>
<%@ page import="com.main.workspace.model.Course" %>
<%@ page import="java.util.List" %>
<%@ page import="com.main.workspace.dao.StudentDAO" %>
<%@ page import="com.main.workspace.model.Student" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String courseId = request.getParameter("courseID");

    CourseDAO courseDAO;
    try {
        courseDAO = new CourseDAO(ConnectionPool.getConnection());
    } catch (SQLException e) {
        throw new RuntimeException(e);
    }

    Course course;
    course = courseDAO.getCourseById(courseId);

    if (course == null) {
        request.setAttribute("errorMessage", "Course not found");
        request.getRequestDispatcher("/error.jsp").forward(request, response);
        return;
    }

    StudentDAO studentDAO;
    try {
        studentDAO = new StudentDAO(ConnectionPool.getConnection());
    } catch (SQLException e) {
        throw new RuntimeException(e);
    }

    List<Student> registeredStudents = studentDAO.getStudentsByCourseId(courseId);
%>
<!DOCTYPE html>
<html>
<head>
    <title>Course Details</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<h2>Course Details</h2>
<p style="text-align: center">Course ID: <%= course.getCourseId() %></p>
<p style="text-align: center">Course Name: <%= course.getCourseName() %></p>

<h2>Student list:</h2>
<table>
    <thead>
    <tr>
        <th>Student ID</th>
        <th>Student Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <% for (Student registerStudent : registeredStudents) { %>
    <tr>
        <td><%= registerStudent.getId() %></td>
        <td><%= registerStudent.getName() %></td>
        <td>
            <form action="${pageContext.request.contextPath}/Course?action=removeStudent&courseID=<%= course.getCourseId() %>" method="post" onsubmit="return confirmRemoveStudent()">
                <input type="hidden" name="studentID" value="<%= registerStudent.getId() %>">
                <input type="hidden" name="courseID" value="<%= course.getCourseId() %>">
                <button type="submit">Remove</button>
            </form>
        </td>
    </tr>
    <% } %>
    </tbody>
</table>
<hr>
<div style="text-align: center">
    <a href="${pageContext.request.contextPath}/">Homepage</a>
</div></body>

<script>
    function confirmRemoveStudent() {
        return confirm('Are you sure you want to remove this student?');
    }
</script>

</html>
