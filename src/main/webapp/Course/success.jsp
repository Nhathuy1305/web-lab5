<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String message = (String) request.getAttribute("message");
%>
<html>
<head>
    <title>Operation Status</title>
    <link rel="stylesheet" type="text/css" href="Course/style.css">
</head>
<body>
<h1 class="message"><%= message %></h1>
<a class="back-link" href="${pageContext.request.contextPath}/Course">Back to Course List</a>
</body>
</html>