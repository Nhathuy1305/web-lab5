<%@ page import="com.main.workspace.model.Course" %>
<%@ page import="com.main.workspace.dao.CourseDAO" %>
<%@ page import="com.main.workspace.util.ConnectionPool" %>
<%@ page import="java.sql.SQLException" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
  String courseId = request.getParameter("id");
  CourseDAO courseDAO;
  try {
    courseDAO = new CourseDAO(ConnectionPool.getConnection());
  } catch (SQLException e) {
    throw new RuntimeException(e);
  }
  Course course = courseDAO.getCourseById(courseId);
%>
<!DOCTYPE html>
<html>
<head>
  <title>Update Course</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<h2>Update Course</h2>
<%
  if (course != null) {
%>
<form class="update-form" action="${pageContext.request.contextPath}/Course?action=update" method="post">
  <input type="hidden" name="id" value="<%= course.getCourseId() %>">
  <label style="text-align: center" for="courseName">Course Name</label><br>
  <input type="text" id="courseName" name="courseName" value="<%= course.getCourseName() %>"><br>
  <input class="submit" type="submit" value="Update">
</form>
<%
} else {
%>
<p>No course found with the given ID.</p>
<%
  }
%>
</body>
</html>