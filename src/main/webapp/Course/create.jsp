<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create Course</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<h2>Create Course</h2>
<form class="create-form" action="${pageContext.request.contextPath}/Course?action=create" method="post">
    <input type="hidden" name="action" value="create">
    <label for="id">Course ID:</label><br>
    <input type="text" id="id" name="id"><br>
    <label for="courseName">Course Name:</label><br>
    <input type="text" id="courseName" name="courseName"><br>
    <input class="create-button" type="submit" value="Create Course">
</form>
</body>
</html>