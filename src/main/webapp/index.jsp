<%@ page import="com.main.workspace.model.Student" %>
<%@ page import="com.main.workspace.dao.StudentDAO" %>
<%@ page import="com.main.workspace.util.ConnectionPool" %>
<%@ page import="java.sql.SQLException" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String studentId = request.getParameter("studentID");
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
    <title>Homepage</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
<h1>Course Registration Demo</h1>
<hr>
<form action="Student/view.jsp" method="get">
    <label style="text-align: center; font-weight: bold" for="studentID">Enter student's ID</label><br>
    <div class="center">
        <input type="text" id="studentID" name="studentID">
    </div>
    <input type="submit" value="Submit">
</form>
<h2>Manager</h2>
<a href="Student">Students Manager</a><br>
<a href="Course">Courses Manager</a>
</body>
</html>